import math
import utime
import lcd

class deque:
    def __init__(self, maxLen):
        self.list = []
        self.maxLen = maxLen
        self.currentLen = len(self.list)

    def insert(self, value):
        if(len(self.list) >= self.maxLen):
            self.list.pop()
        self.list = [value] + self.list

    def avg(self):
        return sum(self.list)/len(self.list)

    def isNotEmpty(self):
        return len(self.list) > 0

    def lastElement(self):
        return self.list[0]

    def clean(self):
        self.list = []


class path:
    def __init__(self, maxLen, maxDist):
        self.path = {'x': deque(maxLen), 'y': deque(maxLen)}
        self.maxDist = maxDist
        self.maxLen = maxLen
        self.timeRef = utime.ticks_ms()

    def feedWatchDog(self):
        self.timeRef = utime.ticks_ms()

    def watchDog(self):
        interval = utime.ticks_diff(utime.ticks_ms(), self.timeRef)
        if(interval > 200):
            #print("Time since last detection: ",interval)
            self.timeRef = utime.ticks_ms()
            self.clean()

    def getPath(self):
        return {'x': self.path['x'].list, 'y': self.path['y'].list}

    def clean(self):
        self.path['x'].clean()
        self.path['y'].clean()

    def isNotEmpty(self):
        return self.path['x'].isNotEmpty() and self.path['y'].isNotEmpty()

    def getLen(self):
        if(len(self.path['x'].list) == len(self.path['y'].list)):
            return len(self.path['x'].list)
        else:
            return 0

    def lastPoint(self):
        return [self.path['x'].list[0], self.path['y'].list[0]]

    def indexedPoint(self, index):
        return [self.path['x'].list[index], self.path['y'].list[index]]

    def distFromLast(self, point = [0,0]):
        dist = math.sqrt((point[0] - self.lastPoint()[0])**2 +(point[1] - self.lastPoint()[1])**2)
        return dist

    def insertPoint(self, point = [0,0]):
        if(self.isNotEmpty()):
            if(self.distFromLast(point) < self.maxDist):
                self.path['x'].insert(point[0])
                self.path['y'].insert(point[1])
            else:
                print("Point not inserted")
        else:
            self.path['x'].insert(point[0])
            self.path['y'].insert(point[1])

class watcher:
    def __init__(self, inBoundary = {'x': [], 'y': []}, outBoundary = {'x': [], 'y': []}):
        self.inBoundary = inBoundary
        self.outBoundary = outBoundary

    def insideBoundary(self, point, boundary):
        return ((point[0] >= boundary['x'][0]) and (point[0] <= boundary['x'][1]) and (point[1] >= boundary['y'][0]) and (point[1] <= boundary['y'][1]))

    def displayBoundaries(self, img):
        inX, inY = self.inBoundary['x'][0], self.inBoundary['y'][0]
        inW, inH = self.inBoundary['x'][1] - inX, self.inBoundary['y'][1] - inY
        outX, outY  = self.outBoundary['x'][0], self.outBoundary['y'][0]
        outW, outH = self.outBoundary['x'][1] - outX, self.outBoundary['y'][1] - outY
        inBox, outBox  = img.draw_rectangle(inX,inY,inW,inH,(0,255,0),thikness=500), img.draw_rectangle(outX,outY,outW,outH,(255,0,0),thikness=500)
    def watch(self, path):
        vIn = self.gotIn(path)
        vOut = self.gotOut(path)
        if(vIn and vOut):
            return 'error'
        elif(vIn and not vOut):
            path.clean()
            return 'in'
        elif(not vIn and vOut):
            path.clean()
            return 'out'
        else:
            return 'none'


    def gotOut(self, path):
        if(path.isNotEmpty() and self.insideBoundary(path.lastPoint(), self.outBoundary)):
            outPoints = 0
            numberOfPoints = path.getLen()
            for i in range(1, numberOfPoints):
                if not(self.insideBoundary(path.indexedPoint(i), self.outBoundary)):
                    outPoints += 1
            return outPoints >= 1
        else:
            return False

    def gotIn(self, path):
        if(path.isNotEmpty() and self.insideBoundary(path.lastPoint(), self.inBoundary)):
            outPoints = 0
            numberOfPoints = path.getLen()
            for i in range(1, numberOfPoints):
                if not(self.insideBoundary(path.indexedPoint(i), self.inBoundary)):
                    outPoints += 1
            return outPoints >= 1
        else:
            return False

    def displayPath(self, path):
        if(path.isNotEmpty()):
            for i in range(path.getLen()):
                lcd.draw_string(int(path.indexedPoint(i)[0]),int(path.indexedPoint(i)[1]),str(path.getLen()-i),255,100)


class tempRunner():
    def __init__(self, timeOut = 10000):
        self.timeRef = 0
        self.timeOut = timeOut
        self.firstRun = True

    def run(self, foo, enable = True):
        if(self.firstRun):
            self.firstRun = False
            self.timeRef = utime.ticks_ms()
        interval = utime.ticks_diff(utime.ticks_ms(), self.timeRef)
        if(interval < self.timeOut):
            if(enable):
                foo()

    def reset(self):
        self.firstRun = True
        print('runner restarted')
